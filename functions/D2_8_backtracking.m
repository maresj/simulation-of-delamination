function[control,solution]=backtracking(matrices,net,parameters,solution,c,u_DNum)
    % Do global matrices
    Kg_Om=matrices.Kd_preloc{1};
    Kg_Gam=matrices.Kd_preloc{1};
    Kg_Gam_min=matrices.Kd_preloc{1};
    for nD=1:net.nDomain
        Kg_Om= Kg_Om+matrices.Kd{nD};     
        Kg_Gam= Kg_Gam+matrices.Kd_omega{c}{nD};               %Stiffness matrix on interface in time t
        Kg_Gam_min= Kg_Gam_min+matrices.Kd_omega{c-1}{nD};     %Stiffness matrix on interface in time t-1
    end
    
%     Energetics functions
    E=@(u,Kg_G,Kg_O) 1/2*(u'*(Kg_G+Kg_O)*u)*1000;
    D=@(omega,omega_m) parameters.Gc*parameters.t*net.hSize*1000*ones(1,net.nInterfaces)*(omega-omega_m);

    U_k=solution.U_t{c};                        % Displacements in step k and time k                                                                 
    U_k_Tmin=U_k;                               % Displacements in step k and time k-1 
    U_k_Tmin(u_DNum*2)=-solution.u_d(c-1);      

    U_k_min=solution.U_t{c-1};                  % Displacements in step k-1 and time k-1
    U_k_min_T=U_k_min;                          % Displacements in step k-1 and time k 
    U_k_min_T(u_DNum*2)=-solution.u_d(c);

    Om_k=solution.Om_t{c};
    Om_k_min=solution.Om_t{c-1};
    Om_k_0=ones(net.nInterfaces,1)*parameters.omega_in;

    E_k=E(U_k,Kg_Gam,Kg_Om);
    E_k_min=E(U_k_min,Kg_Gam_min,Kg_Om);
    D_k=D(Om_k,Om_k_min);
    D_all=D(Om_k,Om_k_0);
    
    % only controling
    left=E_k-E(U_k_Tmin,Kg_Gam,Kg_Om);
    middle= -E_k_min+D_k+E_k;
    right=E( U_k_min_T,Kg_Gam_min,Kg_Om)-E_k_min;

    % real energetic estimate 
        %     left=E_k-E(U_k_Tmin,Kg_Gam,Kg_Om)+E_k_min-D_k+D_all;
        %     middle=E_k+D_all;
        %     right=E(U_k_min_T,Kg_Gam_min,Kg_Om)-D_k+D_all;

    solution.EnergyEq.left(c-1)=left;
    solution.EnergyEq.middle(c-1)=middle;
    solution.EnergyEq.right(c-1)=right;

    control=left<=middle && middle<=right; %;
end
