
function [matrices]=C4_InterfaceBooleanMatrix(nodes,interfaces,nNodes,nInterfaces,matrices)
    % find fixed loc numbers
    fixedNum=find([nodes(:).fixed]==1);
    nFixed=length(fixedNum);
    
    % Boolean matrix for Dirrichlet boundary conditions
    B_u=zeros(nFixed,nNodes*2);
    B_u(1:nFixed,fixedNum)=eye(nFixed);
    
    %Interface Boolean Matrix
    B_I=zeros(nInterfaces+1,nNodes*2);
    for nI=1:nInterfaces
         % rozdíl v normálových dofech
         B_I([nI,nI+1],interfaces(nI).UpperNodes*2)=[1,0;0,1];
         B_I([nI,nI+1],interfaces(nI).LowerNodes*2)=[-1,0;0,-1];
    end

    matrices.B_Ig=sparse(B_I);
    matrices.B_ug=sparse(B_u);
end