function PostProReactionForcePloting(ploting,boundary_con,StepNum,matrices,solution,nNodes,nDomain)
    if ploting.PlotReaction==1
       used=[1:1:StepNum];
       loc=[boundary_con.u_DNum*2,boundary_con.fixedCoords*2];
       n=0;
       for Sn=used
            n=n+1;
            % Deformovaný  tvar kce
            u_sn=solution.U_t{Sn}; 
            u_dn(n)=solution.u_d(Sn);
            
            % Do global StifnessMatrix
            Kk=sparse(zeros(nNodes*2));
            for nD= 1:nDomain
                Kk=Kk+matrices.Kd{nD}+matrices.Kd_omega{n}{nD};
            end
            % Solve internal forces
            B_I=matrices.B_Ig(find(solution.W_k{Sn}),:);
            B=B_I;
            b=size(B);
            lambda=-B*(-Kk*u_sn+matrices.f{Sn})/2; 
            
            K=[Kk(loc,:),-B(:,loc)'];
            Ul=[u_sn;lambda];
            nForce=K*Ul;
            fd(n)=abs(nForce(1));
            rd(n)=nForce(2)+nForce(3);
       end
       %Plot force depending on prescribed displacement
       figure
       hold on
       xlabel('Prescribed displacement')
       ylabel('Reaction in place of prescribed displacement')
       title('Reaction forces')
       plot(u_dn,fd,'Color','red','LineWidth',1,'Marker','.','MarkerSize',8)
       plot(u_dn,rd)
    end
end
