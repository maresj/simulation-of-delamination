function[matrices,locNum]=D2_2_GlobalMatrix(nodes,net,u_d,u_DNum,matrices,Numbers,c)
    % Do global Stiffness matrix   
    Kk=sparse(zeros(net.nNodes*2));
    for nD= 1:net.nDomain
        Kk=Kk+matrices.Kd{nD}+matrices.Kd_omega{c}{nD};
    end
    % Do global displacement vector with prescribed displacements    
    u_0=zeros(net.nNodes*2,1);
    u_0(u_DNum*2,1)=-nonzeros([nodes(u_DNum).u_D])*u_d;
    % Calculate global right side vector
    f=-Kk*u_0;
    matrices.KK=Kk;
    matrices.f{c}=sparse(f);
    % localization without dofs with Neuman and Dirrichlet conditions 
    locNum=Numbers.CalcDOFs;
    matrices.fg=sparse(f(locNum,1));
    matrices.Kg=Kk(locNum,locNum);
    matrices.u_g=sparse(u_0(locNum,1));
    B_I=matrices.B_Ig(:,locNum);
    matrices.B_I=B_I(any(B_I,2),:);
    nI=size(B_I,1);
    matrices.cI=zeros(nI,1);
    
    if net.nNeighbours~=0
        B_N=matrices.B_Ng(:,locNum);
        matrices.B_N=B_N(any(B_N,2),:);
        nN=size(B_N,1);
        matrices.cn=zeros(nN,1);
    else
        matrices.B_N=[];
        matrices.cn=[];
    end
    matrices.c=[matrices.cn;matrices.cI];
    matrices.B=[matrices.B_N;matrices.B_I];
    matrices.u_0=u_0;
end