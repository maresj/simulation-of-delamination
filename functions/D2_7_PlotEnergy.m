function D2_7_PlotEnergy(CalculatioPloting,Energy,c)
    if CalculatioPloting==1
       fig=plot(Energy.Sum(1:c),'color','red','LineWidth',1,'Marker','.');
       fig=plot(Energy.E_Om(1:c),'color','black','LineStyle','-.');
       fig=plot(Energy.E_Gam(1:c),'color',[0.3010 0.7450 0.9330],'LineStyle','-.');
       fig=plot(Energy.E(1:c),'color','blue','LineStyle','-','LineWidth',0.5,'Marker','.');
       fig=plot(Energy.D(1:c),'color','green','LineStyle','--');
       fig=plot(Energy.VarD(1:c),'color',[0.9290 0.6940 0.1250],'LineStyle','-','LineWidth',0.5,'Marker','.');
       
       
       t=title('$\textbf{Energy of solutions}$');
       set(t,'Interpreter','latex');
       leg=legend({'$\mathcal{E}_{k}+\sum_{k=1}^{i}\mathcal{D}_{k}$','$\mathcal{E}_{\Omega,k}$','$\mathcal{E}_{\Gamma_{I},k}$','$\mathcal{E}_{k}$','$\mathcal{D}_{k}$','$\sum_{k=1}^{i}\mathcal{D}$'});
       set(leg,'Interpreter','latex');
       set(leg,'Location','northwest');
       x=xlabel('time');
       y=ylabel('Energy (Nmm)');
       set(x,'Interpreter','latex');
       set(y,'Interpreter','latex');
       drawnow
    end
end