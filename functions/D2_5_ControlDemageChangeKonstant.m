function [control]=D2_5_ControlDemageChangeKonstant(Omega_0,Omega_t,DemageVecTol)
       control=norm(Omega_0-Omega_t)<=DemageVecTol;
end