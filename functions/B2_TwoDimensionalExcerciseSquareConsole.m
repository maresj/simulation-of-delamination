function [excercise,net,boundary_con]=B2_TwoDimensionalExcerciseSquareConsole(net,PlotExcercisse)
%   Excercise geomety  
    l_1= 0.12 ;% [mm] upper beam length 
    l_3= 0.01 ;% [mm]initial crack length
    l_4= 0;% [mm]lower beam indentation
    h_1= 0.003;% [mm] One beam height
    

    HorizontalNodes=l_1/net.hSize+1;
    VerticalNodesOne=h_1/net.vSize+1;
    UpperBeamNodes= HorizontalNodes*VerticalNodesOne;
%   Upper beam nodes function
    n=0;
    for ver=1: VerticalNodesOne
        for hor= 1:HorizontalNodes
            nodes(hor+(ver-1)*HorizontalNodes)=struct('coords',[(hor-1)*net.hSize,(ver-1)*net.vSize,0]','fixed',[0,0],'u_D',[0,0]);
        
        %   Define prescribed displacements locNum
            if  ver== VerticalNodesOne &&  hor==HorizontalNodes 
                u_DNum(1)=hor+(ver-1)*HorizontalNodes;
                nodes(hor+(ver-1)*HorizontalNodes).u_D(2)=-1;
            end
            %   Define fixed dofs
            if hor==1
                n=n+1;
                nodes(hor+(ver-1)*HorizontalNodes).fixed=[1,1];
                fixedCoords(n)=hor+(ver-1)*HorizontalNodes;
            end
        end    
    end
%   Lover beam nodes function
    for ver=1: VerticalNodesOne
        for hor= 1:HorizontalNodes-l_4/net.hSize
            nodes(UpperBeamNodes+hor+(ver-1)*(HorizontalNodes-l_4/net.hSize))=struct('coords',[(hor-1)*net.hSize,- (VerticalNodesOne-1)*net.vSize+(ver-1)*net.vSize,0]','fixed',[0,0],'u_D',[0,0]);
            %   Define fixed dofs
            if hor==1
                n=n+1;
                nodes(UpperBeamNodes+hor+(ver-1)*(HorizontalNodes-l_4/net.hSize)).fixed=[1,1];
                fixedCoords(n)=UpperBeamNodes+hor+(ver-1)*(HorizontalNodes-l_4/net.hSize);
            end
            if  ver== 1 &&  hor==HorizontalNodes 
                u_DNum(2)=UpperBeamNodes+hor+(ver-1)*(HorizontalNodes-l_4/net.hSize);
                nodes(UpperBeamNodes+hor+(ver-1)*(HorizontalNodes-l_4/net.hSize)).u_D(2)=1;
            end
        end
    end
 nNodes=length(nodes);

    
%   square finite elements upper beam
    n=0;
    m=0;
    for verm=1:VerticalNodesOne-1
        for horm=1:HorizontalNodes-1
            N1=horm+(verm-1)*HorizontalNodes;
            N2=N1+1;
            N3=N1+HorizontalNodes;
            N4=N2+HorizontalNodes;
            elements(horm+(verm-1)*(HorizontalNodes-1))=struct('nodes',[N1,N2,N3,N4]);
            %   Define domains elements loc numbers
            if  horm<=HorizontalNodes-1-((l_4+l_3)/net.hSize)
                n=n+1;
                Dom1ElemNum(n)=horm+(verm-1)*(HorizontalNodes-1);
            else
                m=m+1;
                Dom2ElemNum(m)=horm+(verm-1)*(HorizontalNodes-1);
            end
        end
    end
%   square finite elements lower beam
    n=0;
    m=0;
    UpperBeamElements=(VerticalNodesOne-1)*(HorizontalNodes-1);
    for verm=1:VerticalNodesOne-1
        for horm=1:HorizontalNodes-l_4/net.hSize-1
            N1=UpperBeamNodes+horm+(verm-1)*(HorizontalNodes-l_4/net.hSize);
            N2=N1+1;
            N3=N1+(HorizontalNodes-l_4/net.hSize);
            N4=N2+(HorizontalNodes-l_4/net.hSize);
            elements(UpperBeamElements+horm+(verm-1)*(HorizontalNodes-l_4/net.hSize-1))=struct('nodes',[N1,N2,N3,N4]);
            %   Define domains elements loc numbers
            if  horm<=HorizontalNodes-1-((l_4+l_3)/net.hSize)
                n=n+1;
                Dom3ElemNum(n)=UpperBeamElements+horm+(verm-1)*(HorizontalNodes-l_4/net.hSize-1);
            else
                m=m+1;
                Dom4ElemNum(m)=UpperBeamElements+horm+(verm-1)*(HorizontalNodes-l_4/net.hSize-1);
            end    
        end
    end
  nElements=length(elements);

  %   Define domains- Domains are diffined like upper and lower beem and with
  %   contact and with initial crack
      domain(1)=struct('elements',[Dom1ElemNum,Dom2ElemNum],'nDElements',UpperBeamElements,'nDNodes', UpperBeamNodes);
      domain(2)=struct('elements',[Dom3ElemNum,Dom4ElemNum],'nDElements',nElements-UpperBeamElements,'nDNodes',nNodes- UpperBeamNodes); 

      nDomain=length(domain);    

  %   Define interfaces
      for nH=1:HorizontalNodes-l_4/net.hSize-l_3/net.hSize-1
          interfaces(nH)=struct('nodes',[nH,nH+1,nNodes-(HorizontalNodes-l_4/net.hSize)+nH,nNodes-(HorizontalNodes-l_4/net.hSize)+nH+1],'UpperNodes',[nH,nH+1],'LowerNodes',[nNodes-(HorizontalNodes-l_4/net.hSize)+nH,nNodes-(HorizontalNodes-l_4/net.hSize)+nH+1],'Domains',[1,2]);  
      end
      nInterfaces=length(interfaces);
 if PlotExcercisse==1
  %   Square surface excercise ploting
      figure
      hold on
      for nD=1:nDomain
          CoordsND=[nodes([elements(domain(nD).elements).nodes]).coords];
          X=CoordsND(1,:);
          Y=CoordsND(2,:);
          lx=length(X);
          Z=zeros(lx);
          surf(X,Y,Z,'FaceColor',[1-nD/nDomain/3,nD/nDomain/2,nD/nDomain/1],'Facealpha',0.01,'Edgealpha',1,'EdgeColor',[1-nD/nDomain/2,nD/nDomain/2,nD/nDomain/1])
          axis("equal")
      end
      for nI=1:nInterfaces
          N1=interfaces(nI).nodes(1);
          N2=interfaces(nI).nodes(2);
          x=[nodes(N1).coords(1),nodes(N2).coords(1)];
          y=[nodes(N1).coords(2),nodes(N2).coords(2)];
          z=[nodes(N1).coords(3),nodes(N2).coords(3)];
          plot3(x,y,z,'_','Color','green','LineWidth',2)
      end
%       axis off
        fcoords=[nodes(fixedCoords(1:end)).coords];
        plot(fcoords(1,:),fcoords(2,:),'^')
%       plot predescribed displacements
        ucoords=[nodes(u_DNum).coords];
        X= ucoords(1,:);
        Y= ucoords(2,:);
        U=[nodes(u_DNum(1)).u_D(1),nodes(u_DNum(2)).u_D(1)];
        V=-[nodes(u_DNum(1)).u_D(2),nodes(u_DNum(2)).u_D(2)];
        quiver(X,Y,U,V,'color','red','LineWidth',1)
        pause(0.0000000001)
 end

%% posprocessing
    excercise.nodes=nodes;
    excercise.elements=elements;
    excercise.interfaces=interfaces;
    excercise.domain=domain;

    net.nNodes=nNodes;
    net.nElements=nElements;
    net.nInterfaces=nInterfaces;
    net.nDomain=nDomain;

    boundary_con.fixedCoords=fixedCoords;
    boundary_con.u_DNum=u_DNum;
end