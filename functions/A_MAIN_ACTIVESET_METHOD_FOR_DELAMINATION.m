%   This is a script for simulation of delamination problem based with two
%   dimensional finite element discretization.The problem solution is based 
%   on the alternating minimalization algorythm and correction of the 
%   solution using the heuristic energy backtracking presented by A. Mielke,    
%   T. Roubiček and J. Zeman .
%   For solution is used active set method algorythm adjusted for delamination.
%   This script using sparse matrix formating
%   
%   Author: Bc. Jakub Mareš
%   Supervisor: Ing. Martin Doškář, PhD. 
%   Institution: CTU in Prag, Fakulty of civil engeniering
%   date: 2. 1. 2024

%% USER SETTINGS 
    settings.material="ductile"; %material parameters  "brittle or ductile or add your own material"
    settings.excercisse=1;               %or 2
    settings.UseBacktracking=1;          % use backtracking algorithm (1 or 0)
    settings.UpgrageBacktracking=1;
   
% time discretization settings
    settings.discretization.stepsEven=1;                        % Do even steps or steps defined by step vec   
    settings.discretization.TimeStep=0.0000375;                 % Step length for even steps [m] 
    settings.discretization.Umin=0;                             % First step value [m] 
    settings.discretization.StepNum=41;                         % Number of steps
    %not even steps setting
    settings.discretization.NotEven.nIntervals=4;               %Number of intervals is not limited
    settings.discretization.NotEven.InteravalIterations(1)=12;  %How many intervals should it do with step lenght 1 
    settings.discretization.NotEven.InteravalIterations(2)=60;
    settings.discretization.NotEven.InteravalIterations(3)=100; %How many intervals should it do with step lenght 2  
    settings.discretization.NotEven.InteravalIterations(4)=105; %...  
    
    settings.discretization.NotEven.IntervalStepLengthMultiplier(1)=1;      % Step length in first interval is this value times settings.discretization.TimeStep
    settings.discretization.NotEven.IntervalStepLengthMultiplier(2)=1/20;
    settings.discretization.NotEven.IntervalStepLengthMultiplier(3)=1/10;
    settings.discretization.NotEven.IntervalStepLengthMultiplier(4)=1/7;

    [settings.discretization.StepVec,settings.discretization.StepNum]=B0_NotEvenSteps(settings.discretization.NotEven,settings.discretization);

% Numerical tolerance
    settings.numerical.DemageVecTol=1*10^(-6);      % Numerical tolerance for demage change in Alternating minimalization algorythm
    settings.numerical.ActivesetTol=1*10^(-9);      % Numerical tolerance for step length in Active set 
    settings.numerical.nTol=2000;                   % Maximal number of iterations in Alternating minimalization algorithm  
    settings.numerical.nActive=2000;                % Maximal number of iterations in Active set algorithm
    settings.numerical.NumberDeactivated='All';     % or All, It means how much index is deactivated in active set method (7.33)

% Demage aproximation  
    settings.TangentialDemage=1;    %calculate with tengetial demage 

% Finite element net step length  
    net.hSize=0.001; % horizontal
    net.vSize=0.001; % vertical

%% Which plots should be print
    settings.ploting.PlotExcercisse=1;      % plot basic exccercise geometry before calculation    
% energy ploting 
    settings.ploting.Energy.plot=1;
    settings.ploting.Energy.CalculatioPloting=1;

% Solution ploting
    settings.ploting.solution.plot="nop";               % Plot solutions yes or nop
    settings.ploting.solution.animation="yes";
    settings.ploting.solution.AnimationFrameRate=3;     % How much frames in second should be in solution animation
    settings.ploting.AnimationDemage=1;
% backtracking control
    settings.ploting.backtracking=0;
% reaction ploting
    settings.ploting.PlotReaction=1;
% Ploting seting
        settings.ploting.solution.PlotedSteps="manual";     % Plot specific time steps 'automatic or manul'
        settings.ploting.solution.PlotedSteps_Num=[1:41]; %[5,13,53,123,163,194,222,250,277];   % manul ploting steps numbers
        settings.ploting.DeformScal=5;                      % Deformation scaling
        settings.ploting.SolutionPath='solution\';
    
    %calculation ploting settings
        settings.ploting.calc.plotedSteps.c=[10];
        settings.ploting.calc.plotedSteps.j=[1:10];
        settings.ploting.calc.plotedSteps.i=1:20;
        settings.ploting.calc.workingSet=0;
        settings.ploting.calc.SumWorking=0; %Print sum of active conditions in all active set iterations
        settings.ploting.calc.StepLength=0; %Alpha
        settings.ploting.calc.Lam=0;
        settings.ploting.calc.iterations=1;
%% Preprocessing. excercise
tic
    [parameters]=B1_ExcerciseParameters(settings.material);   
    if settings.excercisse==1
        [excercise,net,boundary_con]=B2_TwoDimensionalExcerciseSquareBeam(net,settings.ploting.PlotExcercisse);
        excercise.neighbours=[];
        net.nNeighbours=0;
    elseif settings.excercisse==2
        [excercise,net,boundary_con]=B2_TwoDimensionalExcerciseSquareConsole(net,settings.ploting.PlotExcercisse);
        excercise.neighbours=[];
        net.nNeighbours=0;
    end
time(1)=toc;
%% Preprocessing.matrices
tic
    [calculation,boundary_con]=C1_LocalizationNumbers                  (net.nNodes,boundary_con,excercise.interfaces);
    [matrices]=                C2_OneFiniteElement                     (net,excercise,parameters);
    [matrices.NtN]=            C3_InterfaceStiffnesNumericalIntegration(parameters.t,parameters.beta, ...
                                                                        net.hSize,net.nInterfaces, ...
                                                                        excercise.interfaces,excercise.nodes);
    [matrices]=                C4_InterfaceBooleanMatrix               (excercise.nodes,excercise.interfaces,net.nNodes,net.nInterfaces,matrices);
    [matrices]=                C5_NeighbourBooleanMatrix               (excercise,net,matrices);
time(2)=toc;
%% Initialization 
tic
    [calculation.Omega_t]=D1_InitialDemageVector(parameters.omega_in,net.nInterfaces);  
%% Calculation
    [solution,calculation,matrices,boundary_con]=D2_TimeStepAlgorythm(net,boundary_con,settings,matrices,calculation,excercise,parameters);
time(3)=toc;
%% Postprocessing
tic
    PostProControlCalculationViz(calculation,solution.iterations,settings.ploting.calc);
    %%
    PostProSolutionVizualization(settings,net.nNodes,excercise.nodes,solution);
    %%
    PostProPlotAnimation(settings,net.nNodes,excercise.nodes,solution);
    %%
    PostProPlotAnimationDemage(settings,solution.iterations,calculation.Omega_j,solution.Om_t)
    %%
    PostProPlotEnergy(settings.ploting.Energy.plot,settings.UseBacktracking,solution.u_d,solution.Energy,settings.material,settings.excercisse,settings.ploting.SolutionPath);
    %%
    PostProReactionForcePloting(settings.ploting,boundary_con,settings.discretization.StepNum,matrices,solution,net.nNodes,net.nDomain)
    %%
    PostProPlotBacktracking(settings,solution)
time(4)=toc;

