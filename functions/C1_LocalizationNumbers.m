function [calculation,boundary_con]=C1_LocalizationNumbers(nNodes,boundary_con,interfaces)
    %find all global DOF numbers which have to be calculate
        AllDOFs=1:nNodes*2;
        ConditionsDOFs=[boundary_con.u_DNum*2,boundary_con.fixedCoords(1)*2-1,boundary_con.fixedCoords*2];
        CalcDOFs=AllDOFs(~ismember(AllDOFs,ConditionsDOFs));
        %Renumber
            ReNCalcDOFs=find(CalcDOFs);

    %find interface DOFs
        AllInterfaces=unique([interfaces.nodes]);
        AllIntDOFs=sort([AllInterfaces*2-1,AllInterfaces*2]);
        IntDOFs=AllIntDOFs(~ismember(AllIntDOFs,ConditionsDOFs));
        %Renumber
            ReNIntDOFs=find(ismember(CalcDOFs,IntDOFs));

    %find nonInterfacial DOFs
        AllNonIntDOFs= AllDOFs(~ismember(AllDOFs,AllIntDOFs));
        NonIntDofs=AllNonIntDOFs(~ismember(AllNonIntDOFs,ConditionsDOFs));
        %Renumber
            ReNNonIntDOFs=find(ismember(CalcDOFs,NonIntDofs));

    %find ekvivalent interface Nodes
        UpperNodes=unique([interfaces(:).UpperNodes]);
        LowerNodes=unique([interfaces(:).LowerNodes]);
        IntEqualDOFs=[UpperNodes'*2,LowerNodes'*2]; %only normal DOFs
        %Renumber
            ReNIntEqualDOFs= [find(ismember(CalcDOFs',IntEqualDOFs(:,1))),find(ismember(CalcDOFs',IntEqualDOFs(:,2)))]; 

    %postprocessing
    calculation.Numbers.CalcDOFs=CalcDOFs;
    calculation.Numbers.IntDofs=IntDOFs;
    calculation.Numbers.NonIntDofs=NonIntDofs;

    calculation.Numbers.ReNCalcDOFs=ReNCalcDOFs;
    calculation.Numbers.ReNIntDOFs=ReNIntDOFs;
    calculation.Numbers.ReNNonIntDOFs=ReNNonIntDOFs;

    boundary_con.IntEqualDOFs=IntEqualDOFs;
    boundary_con.ReNIntEqualDOFs=ReNIntEqualDOFs;

end