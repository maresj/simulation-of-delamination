function [NtN]=C3_InterfaceStiffnesNumericalIntegration(t,beta,hSize,nInterfaces,interfaces,nodes)

    % Predefinition
        % Matrix of difference in calculation of displacement jumps
            B=sparse([1,0,-1,0;0,1,0,-1]);
        % Gausian Numerical Integration
            w=[1,1]; % weight functions
            intPoints=[-1/sqrt(3),1/sqrt(3)]; % 2 integration points
%             w=[5/9,8/9,5/9]; % váhové funkce
%             intPoints=[-sqrt(3/5),0,sqrt(3/5)]; % 3 integration points
            nIntP=length(intPoints);
        %  subtitution  
            dxkudxi=hSize/2;
        % prelocalization 
            NtN=cell(1,nInterfaces);
    %  Numerical integration
    for nI=1:nInterfaces
        %       UpperNodes  
                N1=interfaces(nI).nodes(1);
                N2=interfaces(nI).nodes(2);
        %       LowerNodes are identical with UpperNodes

                NtN{nI}=zeros(length(interfaces(nI).nodes(:))*2);
        
                tt=nodes(N2).coords-nodes(N1).coords;
                tt=tt(1:2);
                nn=[-tt(2);tt(1)];
        %       Normalized tangetntial a normal vector
                tnorm=tt/norm(tt);
                nnorm=nn/norm(nn);

                for nInt=1:nIntP
        %           Baze function
                    Nxi1=1/2-1/2*intPoints(nInt);
                    Nxi2=1/2+1/2*intPoints(nInt);
        
                    Nxi3=1/2-1/2*intPoints(nInt);
                    Nxi4=1/2+1/2*intPoints(nInt);   
                    
                    N=[Nxi1,0,Nxi2,0,0,0,0,0;...
                       0,Nxi1,0,Nxi2,0,0,0,0;...
                       0,0,0,0,Nxi3,0,Nxi4,0;...
                       0,0,0,0,0,Nxi3,0,Nxi4];
                            
                    NtNi=t*N'*B'*(nnorm*nnorm'+beta^(2)*(tnorm*tnorm'))*B*N*dxkudxi;
                    NtN{nI}=NtN{nI}+w(nInt)*NtNi;
                end
    end 
end
