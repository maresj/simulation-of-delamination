function [StepVec,StepNum]=B0_NotEvenSteps(NotEven,discretization)
    StepVec=[];
    StepNum=discretization.StepNum;
    if discretization.stepsEven==0
    % control setting
        if NotEven.nIntervals<length(NotEven.InteravalIterations)
            fprintf('There is more interval iterations settings, than how much intervals we have.\n')
        elseif NotEven.nIntervals>length(NotEven.InteravalIterations)
            fprintf(['There is less interval iterations settings, than how much intervals we have.\n' ...
                'Pleas set up more interval iterations numbers.\n'])
            return
        end
    % Do Step Vector
        u_0=discretization.Umin;
        n=1;
        StepVec=zeros(1,sum(NotEven.InteravalIterations)+1);
        for nI=1:NotEven.nIntervals
            StepVec(n+1:n+NotEven.InteravalIterations(nI)-1+1)=u_0+(discretization.TimeStep*NotEven.IntervalStepLengthMultiplier(nI):discretization.TimeStep*NotEven.IntervalStepLengthMultiplier(nI):discretization.TimeStep*NotEven.IntervalStepLengthMultiplier(nI)*NotEven.InteravalIterations(nI));
            u_0=StepVec(n+NotEven.InteravalIterations(nI)-1+1);
            n=n+NotEven.InteravalIterations(nI);
        end
        StepNum=sum(NotEven.InteravalIterations);
    end
end