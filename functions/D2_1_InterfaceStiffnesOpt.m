function [Kd_omega, Ke_I,kk_omega]=D2_1_InterfaceStiffnesOpt(interfaces,nInterfaces,NtN,Kd_preloc,Omega_t,sigma_c,delta_c)
    % Do complete interface matrix from matrices integrated in C3.
    % Aproximation of omega is constant only
    Ke_I=cell(1,nInterfaces);
    Kd_omega=Kd_preloc;
    for nI=1:nInterfaces
        om=Omega_t(nI);
        kk_omega=sigma_c*(1-om)/(delta_c*om); % demage dependent stiffness 
        Ke_I{nI}=kk_omega*NtN{nI};
        Ke_I{nI}=1/2*(Ke_I{nI}+Ke_I{nI}');    % symetrization for numerical optimalization
        loc1=sort([interfaces(nI).UpperNodes*2-1,interfaces(nI).UpperNodes*2]);
        loc2=sort([interfaces(nI).LowerNodes*2-1,interfaces(nI).LowerNodes*2]);
        Domains=interfaces(nI).Domains;
        Kd_omega{Domains(1)}([loc1,loc2],loc1)=Kd_omega{Domains(1)}([loc1,loc2],loc1)+Ke_I{nI}(:,1:4);
        Kd_omega{Domains(2)}([loc1,loc2],loc2)=Kd_omega{Domains(2)}([loc1,loc2],loc2)+Ke_I{nI}(:,5:8);
    end 
end