function PostProPlotAnimationDemage(settings,iterations,Omega_j,Om_t)
    if settings.ploting.AnimationDemage==1
        figure 
        n=0;
        for c=1:iterations.c 
            for j=1:iterations.j{c}
                clf
                hold on
                stem(Omega_j{c,j},'LineStyle',':','LineWidth',0.1,'color','blue','Marker','.')
                plot(Omega_j{c,j},'LineWidth',1,'color','blue')
                if c>1
                    h=0:4:c;
                    for l=1:c-1
                       if any(l==h) 
                            plot(Om_t{l},'LineWidth',1,'color','r');
                       else
                            plot(Om_t{l},'LineWidth',0.2,'color','black');
                       end
                    end
                end
                xlim([-1,120]);
                ylim([-0.1,1.1]);
                txt=['$\textbf{Vyvoj poskozeni na hranici}$ $g=$',num2str(c)];
                t=title(txt);
                set(t,'Interpreter','latex');
                n=n+1;
                movieVector(n)=getframe (gcf);
            end
        end
       if settings.UseBacktracking==1
            txt2=', Use Backtracking';
       else
           txt2=', No Bactracking';
       end
        txt=convertCharsToStrings(settings.ploting.SolutionPath)+'Demage animation material '+ settings.material+ ', excercisse '+settings.excercisse+txt2;
        myWriter=VideoWriter(txt);
        myWriter.FrameRate=20;
    
        open(myWriter);
        writeVideo(myWriter, movieVector);
        close(myWriter);
    end
end