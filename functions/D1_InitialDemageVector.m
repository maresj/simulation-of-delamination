function [Omega_t]=D1_InitialDemageVector(omega_in,nInterfaces)  
    % initial interface demage vector with konstant aproximation
        Omega_t=omega_in*ones(nInterfaces,1);
end