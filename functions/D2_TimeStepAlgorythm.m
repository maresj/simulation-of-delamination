function [solution,calculation,matrices,boundary_con]=D2_TimeStepAlgorythm(net,boundary_con,settings,matrices,calculation,excercise,parameters)
    %%  prelocalization
        DoBactrac=0;
        c=0;
        if settings.ploting.Energy.CalculatioPloting==1
            figure
            hold on
        end
        calculation.ActiveSetInfo{settings.discretization.StepNum,settings.numerical.nTol}=[];
    %%  Iteration
        if settings.discretization.stepsEven==1
            uuD=settings.discretization.Umin+settings.discretization.TimeStep*(0:settings.discretization.StepNum-1);
        else 
            uuD=settings.discretization.Umin+settings.discretization.StepVec;
        end
        Omega_inc=calculation.Omega_t;          %write demage from step t-1
        while c<settings.discretization.StepNum
            c=c+1;
            u_d=uuD(c);
           
            for j=1:settings.numerical.nTol
                %% Preprocessing
                Omega_0=calculation.Omega_t;    %write demage from step j-1 
                %Solve actual K(omega)
                [matrices.Kd_omega{c}, matrices.Ke_I{c}]=D2_1_InterfaceStiffnesOpt(excercise.interfaces,net.nInterfaces,matrices.NtN,matrices.Kd_preloc,Omega_0,parameters.sigma_c,parameters.delta_c);
                %Globalize problem
                [matrices,boundary_con.locNum]=D2_2_GlobalMatrix(excercise.nodes,net,u_d,boundary_con.u_DNum,matrices,calculation.Numbers,c);                
                %% calculation-Active Set
                % Initial active set node
                if c==1
                    calculation.x_k=matrices.u_g;
                    calculation.W_k=ones(size(matrices.B_I,1),1);
                end
                % calculate
                [calculation.x_k,calculation.W_k,calculation.ActiveSetInfo{c,j}]=D2_3_ActiveSetMethod(calculation.x_k,calculation.W_k,...
                                                                                                matrices.Kg,matrices.fg,matrices.B,matrices.c,...
                                                                                                settings.numerical.nActive,settings.numerical.ActivesetTol,settings.numerical.NumberDeactivated,j);
                %% calculation Demage sulution
                  [calculation.Omega_t]=D2_4_DisipativeEnergyMinimalizationKonstant(excercise.interfaces,excercise.nodes,...
                                                                      net.nInterfaces,net.nNodes,net.hSize,...
                                                                      calculation.x_k,calculation.Numbers.CalcDOFs,Omega_inc,...
                                                                      parameters.beta,parameters.delta_c,boundary_con.u_DNum,u_d,settings.TangentialDemage);
                  calculation.Omega_j{c,j}=calculation.Omega_t;
                %% controling and Solution renumbering
                % controling (control if demage has changed) 
                [control]=D2_5_ControlDemageChangeKonstant(Omega_0,calculation.Omega_t,settings.numerical.DemageVecTol);
                    solution.iterations.c=c;
                    solution.iterations.j{c}=j;

                if  control==1
                    solution.U_t{c}=zeros(net.nNodes*2,1);
                    solution.U_t{c}(calculation.Numbers.CalcDOFs,1)=calculation.x_k;
                    solution.U_t{c}=solution.U_t{c}-u_d*[excercise.nodes.u_D]';
                    solution.Om_t{c}=calculation.Omega_t;
                    solution.W_k{c}=calculation.W_k;
                    solution.u_d(c)=u_d;
                    [solution]=D2_6_SolveEnergy(excercise,matrices,net,parameters,settings,calculation,solution,c,Omega_inc);
                    if settings.ploting.Energy.CalculatioPloting==1
                        D2_7_PlotEnergy(settings.ploting.Energy.CalculatioPloting,solution.Energy,c);
                    end

                    %% Backtracking 
                    % solve energy two sided inequality
                    omega_bactrack=calculation.Omega_t;
                    if c>2
                        if settings.UseBacktracking==1
                           
                            [control2,solution]=D2_8_backtracking(matrices,net,parameters,solution,c,boundary_con.u_DNum);
                            if settings.UpgrageBacktracking==1
                                if control2~=1
                                   DoBactrac=1;  
                                end
                                if solution.Energy.Sum(c)>=solution.Energy.Sum(c-1)
                                    DoBactrac=0;
                                end
                                if DoBactrac==1
                                   c=c-2;
                                   omega_bactrack=solution.Om_t{c};
                                end
                            else
                                if control2~=1
                                   c=c-2;
                                   omega_bactrack=solution.Om_t{c};
                                end
                            end  
                        end
                    end

                    Omega_inc=omega_bactrack; %write demage from step t-1
                    break
                end 
                if j==settings.numerical.nTol
                    solution.txt='solution not asigned';
                    fprintf('It was reached maximal number of iterations in one time step (%d), max. number of iterations is %d \n',c,settings.numerical.nTol)
                end          
            end
        end  
%  For Energy controling figure saving       
%         txt='Energy calculation control';
%         myWriter=VideoWriter(txt);
%         myWriter.FrameRate=5;
%     
%         open(myWriter);
%         writeVideo(myWriter, movieVector);
%         close(myWriter);
end
