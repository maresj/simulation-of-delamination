function [parameters]=ExcerciseParameters(material)
    parameters.E=75*10^9; % Pa
    parameters.ny=0.3;
    parameters.t=0.02; %[m] thicknes 
    
    Gc_Ductile=250;             % J*m^(2)
    omega_in_Ductile=0.001;     % initial demage
    sigma_c_Ductile=5000000;    % critical stress [Pa]

    Gc_Brittle=80;             % J*m^(2)
    omega_in_Brittle=0.001;    % initial demage
    sigma_c_Brittle=130000000; % critical stress [Pa]

    Gc_Testing=100;             % J*m^(2)
    omega_in_Testing=0.001;     % initial demage
    sigma_c_Testing= 5000000;   % critical stress [Pa]

    % Material library
    if material=="ductile"
        parameters.Gc=Gc_Ductile;
        parameters.omega_in=omega_in_Ductile;
        parameters.beta=1;
        parameters.sigma_c= sigma_c_Ductile;
    elseif material=="brittle"
        parameters.Gc=Gc_Brittle;
        parameters.omega_in=omega_in_Brittle;
        parameters.beta=1;
        parameters.sigma_c=sigma_c_Brittle;
   elseif material=="testing"
        parameters.Gc=25;
        parameters.omega_in=omega_in_Testing;
        parameters.beta=1;
        parameters.sigma_c=sigma_c_Testing;
    else
        fprintf('Material is not in material library. Change material or open ExcerciseParameters.mat and include informations /n')
        return 
    end

    parameters.delta_c=2*parameters.Gc/parameters.sigma_c; %m/s?
end