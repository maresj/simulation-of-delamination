function [x_k,W_k,ActiveSetInfo]=D2_3_ActiveSetMethod(x_k,W_k,K,f,B,c,nActive,ActivesetTol,NumberDeactivated,j)
%     x_k = quadprog(K,-f,-B,-c); %Alternative solution
    %% Prelocalization
        ActiveSetInfo.p_k{nActive}=[];        % Step vector
        ActiveSetInfo.x_k{nActive}=[];        % iterated displacement vectors
        ActiveSetInfo.Working{nActive}=[];    % active conditions
        ActiveSetInfo.Lambda{nActive}=[];     % normal interfacial forces
        ActiveSetInfo.Alpha(nActive)=0;       % Active set step length
    %% Iteration
    nInterfaces=length(W_k);
    nNeighbours=size(B,1)-nInterfaces;
%     hold on
    for nA=1:nActive
        [B_underroof,nWorking]=D2_3_1_ActualizeWorking(B,W_k); % Reduce Boolean matrix only on active conditions
        [p_k,lambda]= D2_3_2_SolveQuadraticSubProblem(x_k,K,f,B_underroof,nWorking);    % Solve quadratic subproblem like lagrangian        
        % save calculation info
            ActiveSetInfo.p_k{nA}=p_k;        % Step vector
            ActiveSetInfo.x_k{nA}=x_k;        % iterated displacement vectors
            ActiveSetInfo.Working{nA}=W_k;    %active conditions
        % control if vector p_k is close to 0
        if abs(p_k)<=ActivesetTol
            %if yes control interfacial forces and if some condition is in
            %tension, delete this condition from working set.
            % save calculation info
                ActiveSetInfo.Lambda{nA}=lambda;
            if lambda(nNeighbours+1:end)>=0 % tension is negative
                break
            else % switch condition/s out of working set
                if NumberDeactivated=='All' %user setting of algorihm
                    k_min_all=lambda(nNeighbours+1:end)<=0;
                    Workin=find(W_k==1);
                    renumbering=Workin(k_min_all);
                    W_k(renumbering)=zeros(size(renumbering));
                else %switch only one condition
                    k_min=min(lambda(nNeighbours+1:end))==lambda;
                    Workin=find(W_k==1);
                    renumbering=Workin(k_min);
                    W_k(renumbering)=0;                    
                end
            end
            ActiveSetInfo.Alpha(nA)=-0.1;   %only that it wasn't calculated

        else %if vector p isn't zero find feasible step length
            num1=[~ones(nNeighbours,1);~W_k];
            num2=B*p_k<=0;
            Num=num1 & num2;
            B_num=B(Num,:); 
            c_num=c(Num,:);
            alpha_All_k=(c_num-B_num*(x_k))./(B_num*p_k); % odfiltrovat podmínky B_num*pk>0!!
            if isempty(alpha_All_k)
                alpha_k=1;
            else
                alpha_k=min(1,min(alpha_All_k));
                alpha_k=max(0,alpha_k); 
            end
    
            x_k=x_k+alpha_k*p_k; % update xk
            % find blocking constrains and switch conditions in working set
            if alpha_k~=1
                blockedDof=alpha_All_k<=alpha_k;
                FNum=find(Num(nNeighbours+1:end));
                renumber=FNum(blockedDof);
                W_k(renumber)=ones(length(renumber),1);
            end
            ActiveSetInfo.Alpha(nA)=alpha_k;
            ActiveSetInfo.Lambda{nA}=[]; %only that it wasn't calculated
            
            if nA==nActive
               ActiveSetInfo.txt='solution was not reached ';
               fprintf('It was reached maximal number of iterations in Active set method (j=%d), max. number of iterations is %d \n',[j,nActive]) 
            end
        end
    end
    %% Postprocessing
    ActiveSetInfo.iterations=nA;

    ActiveSetInfo.p_k=ActiveSetInfo.p_k(1:nA);             % Step vector
    ActiveSetInfo.x_k=ActiveSetInfo.x_k(1:nA);             % iterated displacement vectors
    ActiveSetInfo.Working=ActiveSetInfo.Working(1:nA);     %active conditions
    ActiveSetInfo.Lambda=ActiveSetInfo.Lambda(1:nA);       % normal interfacial forces
    ActiveSetInfo.Alpha=ActiveSetInfo.Alpha(1:nA);
    
    
end
