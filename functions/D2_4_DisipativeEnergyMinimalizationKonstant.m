function [Omega_t]=D2_4_DisipativeEnergyMinimalizationKonstant(interfaces,nodes,nInterfaces,nNodes,hSize,x_k,CalcDOFs,Omega_0,beta,delta_c,u_Dnum,u_d,TangentialDemage)
     %This is function which calculate actual demage from disipativ energi
     %minimalization. It means from d/dw (E+D)=0;

     % Gusian numerical integration in two nodes
        w=[1,1]; % weight functions
        intPoints=[-1/sqrt(3),1/sqrt(3)]; %integration points
        nIntP=length(intPoints);
     %  subtitution  
        dxkudxi=hSize/2;
     % renumber x vector
        x_t=zeros(nNodes*2,1);
        x_t(CalcDOFs)=x_k;
        x_t(u_Dnum*2)=-u_d;
     % B matrix for Jump calculation 
     if TangentialDemage==1
        B_i=[1,0,-1, 0;...
             0,1, 0,-1];
     else   
        B_i=[0,0,0, 0;...
             0,1, 0,-1];
     end
     % prelocalization
        du2xi=zeros(1,nIntP);
        Omega_t=zeros(nInterfaces,1);
     % numerical integration and calculation on one iterface    
    for nI=1:nInterfaces
%           Interface finite element nodes  
            N1=interfaces(nI).nodes(1);
            N2=interfaces(nI).nodes(2);
            N3=interfaces(nI).nodes(3);
            N4=interfaces(nI).nodes(4);
            locNum=[N1*2-1,N1*2,N2*2-1,N2*2,N3*2-1,N3*2,N4*2-1,N4*2];
%           tangencial vector
            tt=nodes(N2).coords-nodes(N1).coords;
            tt=tt(1:2);
            t=tt/norm(tt);
%           normal vector 
            n=[t(2);-t(1)];
            for nInt=1:nIntP
                % Baze functions
                Nxi1=1/2-1/2*intPoints(nInt);
                Nxi2=1/2+1/2*intPoints(nInt);
        
                Nxi3=1/2-1/2*intPoints(nInt);
                Nxi4=1/2+1/2*intPoints(nInt);   
                    
                N=[Nxi1,0,Nxi2,0,0,0,0,0;...
                   0,Nxi1,0,Nxi2,0,0,0,0;...
                   0,0,0,0,Nxi3,0,Nxi4,0;...
                   0,0,0,0,0,Nxi3,0,Nxi4];
                du2xi(nInt)=x_t(locNum)'*N'*B_i'*(n*n'+beta^(2)*(t*t'))*B_i*N*x_t(locNum)* dxkudxi;     
            end
            du2=w*du2xi';
%           calculate interface omega_t
            Omega_t(nI,1)=sqrt(du2/(delta_c^(2)*hSize));
            if  Omega_t(nI)<=Omega_0(nI)
                Omega_t(nI)=Omega_0(nI);
            end
            if Omega_t(nI)>=1
               Omega_t(nI)=1;
            end
     end
 end