    function [matrices]=C5_NeighbourBooleanMatrix(excercisse,net,matrices)
        if net.nNeighbours==0
            matrices.B_Ng=[];
        else
            Bng(net.nNeighbours,net.nNodes*2)=sparse(0);
            for nNg=1:net.nNeighbours
                N1=excercisse.neighbours(nNg).nodes(1);
                N2=excercisse.neighbours(nNg).nodes(2);
                Bng([nNg*2-1,nNg*2],[N1*2-1,N1*2])=[1,0;0,1];
                Bng([nNg*2-1,nNg*2],[N2*2-1,N2*2])=[-1,0;0,-1];
            end
            matrices.B_Ng=Bng;
        end
    end 