function [p_k,lambda]=D2_3_2_SolveQuadraticSubProblem(x_k,K,f,B_underroof,nWorking)
        b=length(f);
        Z=zeros(nWorking);
        z=zeros(nWorking,1);
        % Automatic scaling of matrix B
        dK=diag(K);
        maxK=max(dK);
        minK=min(dK);
        scal=(maxK+minK)/2;
        B=scal*B_underroof;
        % Solve problem from Lagrangian derivative
        M= [K,-B';-B,Z];
        V=[f-K*x_k;z];
        S=M\V;
        p_k=S(1:b,1); 
        lambda=S(b+1:end,1);
        
%         p_k = quadprog(K,-f+K*x_k,[],[],B,z); %Alternative solution
end