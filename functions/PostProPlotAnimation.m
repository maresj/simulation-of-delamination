function PlotAnimation(setings,nNodes,nodes,solution)
    if setings.ploting.solution.animation=="yes"
       PlotedSteps=setings.ploting.solution.PlotedSteps;
       DeformScal=setings.ploting.DeformScal;
       PlotedSteps_Num=setings.ploting.solution.PlotedSteps_Num;
       u_max=solution.u_d(end);
       %Plot comparison of useful solutions

       if PlotedSteps=="manual"
          used=PlotedSteps_Num;
       else
          used=[1,5:5:setings.discretization.StepNum];
       end
       figure 
       t=tiledlayout(2,1);
       title(t,'Deformovaný tvar konstrukce a poškození')
       n=0;
%%     
       for Sn=used
            n=n+1;
            u_sn=solution.U_t{Sn}; 
            u_dn=-solution.u_d(Sn);
            m=1;
            o=1;

            X1(m,o)=nodes(1).coords(1)+DeformScal*u_sn(1);
            Y1(m,o)=nodes(1).coords(2)+DeformScal*u_sn(2);
            Z1(m,o)=sqrt(u_sn(1)^2+u_sn(2)^2);
            for nN= 2:nNodes
                if nodes(nN-1).coords(2)~=nodes(nN).coords(2)
                   m=m+1;
                   o=0;
                end
                o=o+1;
                if m<=4
                    X1(m,o)=nodes(nN).coords(1)+DeformScal*u_sn(nN*2-1);
                    Y1(m,o)=nodes(nN).coords(2)+DeformScal*u_sn(nN*2);
                    Z1(m,o)=sqrt(u_sn(nN*2)^2+u_sn(nN*2-1)^2);
                end
                if m>4
                    X2(m-4,o)=nodes(nN).coords(1)+DeformScal*u_sn(nN*2-1);
                    Y2(m-4,o)=nodes(nN).coords(2)+DeformScal*u_sn(nN*2);
                    Z2(m-4,o)=sqrt(u_sn(nN*2)^2+u_sn(nN*2-1)^2);
                end
            end
            clf
            nexttile(1)

            hold on
            mesh(X1,Y1,Z1)
            mesh(X2,Y2,Z2)
            hold off
            txt=['u_{f}=',num2str(u_dn),'m, t=',num2str(-u_dn/u_max)];
            title(txt)
            axis equal
            xlim([-0.001, 0.121])
            ylim([-0.025, 0.01])
            xlabel('x [m]')
            ylabel('y [m]')
            set(gca, 'YTickMode', 'manual');
            set(gca, 'YTickLabel', get(gca,'YTick'));

            % Poškození na rozhraní
            nexttile(2)
            Om=solution.Om_t{Sn}*100;
            bar(Om);
            title(txt)
            xlim([-1, 121])
            xlabel('x [mm]')
            ylabel('\omega(%)')
            xlim([-1, 121])
            ylim([-10, 110])
            movieVector(n)=getframe (gcf);
       end
       if setings.UseBacktracking==1
            txt2=', Use Backtracking';
       else
           txt2=', No Bactracking';
       end
       txt=convertCharsToStrings(setings.ploting.SolutionPath)+'Delamination of material '+ setings.material+ ', excercisse '+setings.excercisse+txt2;
       myWriter=VideoWriter(txt);
       myWriter.FrameRate=setings.ploting.solution.AnimationFrameRate;

       open(myWriter);
       writeVideo(myWriter, movieVector);
       close(myWriter);
    end
end