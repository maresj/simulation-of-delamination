 function [B_underroof,nWorking]=D2_3_1_ActualizeWorking(B,W_i)
        nI=length(W_i);
        n=size(B,1);
        m=n-nI;
        CNum=m+find(W_i==1);
        nWorking=m+length(CNum);
        nNv=1:m;
        B_underroof=B([nNv';CNum],:);
 end