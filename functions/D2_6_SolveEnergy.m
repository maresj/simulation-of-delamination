function [solution]=D2_6_SolveEnergy(excercise,matrices,net,parameters,settings,calculation,solution,c,Omega_inc)
                    % Solve step Energy
                    matrices.Kg_Om(net.nNodes*2,net.nNodes*2)=sparse(0);
                    matrices.Kg_Gam(net.nNodes*2,net.nNodes*2)=sparse(0);
                    [matrices.Kd_omega{c}, matrices.Ke_I{c}]=D2_1_InterfaceStiffnesOpt(excercise.interfaces,net.nInterfaces,matrices.NtN,matrices.Kd_preloc,calculation.Omega_t,parameters.sigma_c,parameters.delta_c);
                    for nD=1:net.nDomain
                        matrices.Kg_Om= matrices.Kg_Om+matrices.Kd{nD};     
                        matrices.Kg_Gam= matrices.Kg_Gam+matrices.Kd_omega{c}{nD};
                    end
                    E_Om=@(u) 1/2*(u'*matrices.Kg_Om*u)*1000; % Nm=1000 Nmm
                    E_Gam=@(u) 1/2*(u'*matrices.Kg_Gam*u)*1000;
                    E=@(u) 1/2*(u'*(matrices.Kg_Gam+matrices.Kg_Om)*u)*1000;
                    D=@(omega) parameters.Gc*parameters.t*net.hSize*1000*ones(1,net.nInterfaces)*(omega- Omega_inc);
                    varD=@(omega)parameters.Gc*parameters.t*net.hSize*1000*ones(1,net.nInterfaces)*(omega-parameters.omega_in*ones(net.nInterfaces,1));

                    solution.Energy.E_Om(c)=E_Om(solution.U_t{c}); %
                    solution.Energy.E_Gam(c)=E_Gam(solution.U_t{c});
                    solution.Energy.E(c)=E(solution.U_t{c});
                    solution.Energy.D(c)=D(calculation.Omega_t);
                    solution.Energy.VarD(c)=varD(calculation.Omega_t);
                    solution.Energy.Sum(c)=solution.Energy.E(c)+solution.Energy.VarD(c);
end