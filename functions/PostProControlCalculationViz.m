function PostProControlCalculationViz(calculation,iterations,calc)
    % chech settings corectnes
    c=calc.plotedSteps.c(calc.plotedSteps.c<=iterations.c); % ploted load cycles
    for aa=c
        j{aa}=calc.plotedSteps.j(calc.plotedSteps.j<=iterations.j{aa}); % Ploted TimeStep Discretization iterations
        for bb=j{aa}
            i{aa,bb}=calc.plotedSteps.i(calc.plotedSteps.i<=calculation.ActiveSetInfo{aa,bb}.iterations); % Active set iterations
        end
    end

    %   Print working set calculation
    if calc.workingSet==1
        for aa=c
            for bb=j{aa}
                figure
                hold on
                t=tiledlayout('flow');
                n=0;
                for cc=i{aa,bb}
                    n=n+1;
                    if  any(n==10:10:calculation.ActiveSetInfo{aa,bb}.iterations)
                        figure
                        hold on
                        t=tiledlayout('flow');
                    end
                    nexttile
                    bar(calculation.ActiveSetInfo{aa,bb}.Working{cc})
                    ylim([-0.1,1.25]) ;
                    xlim([-0.1,100]) ;
                    txt=['Active Set iteration= ',num2str(cc)];
                    title(txt)
                end
                txt2=['Working set in Time Step= ',num2str(aa),' and Algorithm iteration= ',num2str(bb)'];
                title(t,txt2);
            end            
        end
    end

    % Print step lengths in iterations of active set
    if calc.StepLength==1
        for aa=c
            figure
            t=tiledlayout('flow');
            for bb=j{aa}
                nexttile
                hold on
                num=find(calculation.ActiveSetInfo{aa,bb}.Alpha>=0);
                plot(calculation.ActiveSetInfo{aa,bb}.Alpha,'Color','blue','LineStyle',':')
                plot(num,calculation.ActiveSetInfo{aa,bb}.Alpha(calculation.ActiveSetInfo{aa,bb}.Alpha>=0),'+','Color','red','LineWidth',1)
                yline(0);
                ylim([-0.2,1.1]);
                txt=['Algorithm iteration= ',num2str(bb)];
                title(txt)
            end  
            txt=['Active set step length in Time step= ',num2str(aa)];
            title(t,txt);
        end
    end

    % print all calculated Lambda
    if calc.Lam==1
        for aa=c
            for bb=j{aa}
                figure
                t=tiledlayout('flow');
                for cc=i{aa,bb}
                    if ~isempty(calculation.ActiveSetInfo{aa,bb}.Lambda{cc})
                        nexttile
                        hold on
                        Num=find(calculation.ActiveSetInfo{aa,bb}.Working{cc}>0);
                        NumPlus=calculation.ActiveSetInfo{aa,bb}.Lambda{cc}>=0;
                        stem(Num(NumPlus),calculation.ActiveSetInfo{aa,bb}.Lambda{cc}(NumPlus),'Color','green','Marker','.','LineWidth',1)
                        stem(Num(~NumPlus),calculation.ActiveSetInfo{aa,bb}.Lambda{cc}(~NumPlus),'Color','red','Marker','.','LineWidth',1)
                        xlim([-0.1,100]) ;
                        txt=['Active Set iteration= ',num2str(cc)];
                        title(txt);
                    end
                end 
%                 txt2=['\lambda (interfacial normal forces) in Time Step= ',num2str(aa),' and Algorithm iteration= ',num2str(bb)'];
%                 title(t,txt2);
            end
        end
    end

    % print sum of active set
    if calc.SumWorking==1
        for aa=c
            figure
            t=tiledlayout('flow');
            for bb=j{aa}
                nexttile
                hold on
                for lw=1:calculation.ActiveSetInfo{aa,bb}.iterations
                    sW(lw)=sum(calculation.ActiveSetInfo{aa,bb}.Working{lw});
                end
                plot(sW)
                txt=['Algorithm iteration= ',num2str(bb)];
                title(txt)
            end  
            txt=['Sum of active constrains in time step=',num2str(aa)];
            title(t,txt);
        end
    end
    
    % Print numbers of iterations
    if calc.iterations==1
        figure
        hold on
        n=0;
        for a=1:iterations.c
                i=zeros(1,iterations.j{a});
                X=a*ones(1,iterations.j{a});
                Y=1:iterations.j{a};
            for b=1:iterations.j{a}
                i(b)=calculation.ActiveSetInfo{a,b}.iterations;
                n=n+1;
                it(n)=i(b);
            end
            stem3(X,Y,i,'.','LineStyle',':','Color','red','LineWidth',0.25)
            plot3(X,Y,i,'LineStyle','-','LineWidth',2)
            plot3(X,Y,zeros(1,length(i)),'LineStyle',':','LineWidth',0.25,'Color','red')
            xlabel('c-time steps')
            ylabel('j-algorythm steps')
            zlabel('i-active set steps')
            grid on
            view(-20,70);
        end
    end
end