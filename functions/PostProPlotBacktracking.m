function PostProPlotBacktracking(settings,solution)
    if  settings.ploting.backtracking==1
    % plot Backtracking
        x=1/(settings.discretization.StepNum-1):1/(settings.discretization.StepNum-1):1;
        figure
        hold on
        plot(x,solution.EnergyEq.left,'Color',[1, 0.8431,0.0549],'LineStyle','-','LineWidth',1,'Marker','.','MarkerSize',8)
        plot(x,solution.EnergyEq.middle,'Color','r','LineStyle','-','LineWidth',1,'Marker','.','MarkerSize',8)
        plot(x,solution.EnergyEq.right,'Color',[1,0.5294, 0.0549],'LineStyle','-','LineWidth',1,'Marker','.','MarkerSize',8)
        legend('left', 'middle','right','Location','northwest')
        title('Backtracking energy control')
    end
end