function [matrices]=C2_OneFiniteElement(net,excercise,parameters)
    domain=excercise.domain;
    elements=excercise.elements;
    nodes=excercise.nodes;
    
    E=parameters.E;
    ny=parameters.ny;

    %Rovinná deformace
    %     D=E/((1+ny)*(1-2*ny))*[1-ny, ny,0;...
    %                   ny, 1-ny, 0;...
    %                   0,0,(1-2*ny)/2 ];
    %Rovinná napjatost
    D=E/(1-ny^2)*[1, ny,0;...
                  ny, 1, 0;...
                  0,0,(1-ny)/2 ];
    
    %   Pro numerickou integraci bude užito Gausseho pravidlo        
    w=[1,1,1,1]; % váhové funkce
    intPoints=[-1/sqrt(3),-1/sqrt(3);...
                1/sqrt(3),-1/sqrt(3);...
                -1/sqrt(3),1/sqrt(3);...
                1/sqrt(3),1/sqrt(3)]; % 2 integrační body
    nIntP=length(intPoints);

    %matice pro součet derivací podle x a y
    T=[1,0,0,0;...
       0,0,0,1;...
       0,1,1,0];

    J=[net.hSize/2,0;0,net.vSize/2]; % Jakobián je nezávyslí na souřadnicích 
    Jinv=1/det(J)*[net.vSize/2,0;0,net.hSize/2];

    % prelocalizace
    dNmat=cell(1,nIntP);
    KintP=cell(1,nIntP);
    for nE=1
        ENodes=elements(nE).nodes;
        % Součet
        NCoords(:,1)=nodes(ENodes(1)).coords;
        NCoords(:,2)=nodes(ENodes(2)).coords;
        NCoords(:,3)=nodes(ENodes(3)).coords;
        NCoords(:,4)=nodes(ENodes(4)).coords;

        nENodes=length(NCoords);  %Počet bodů v prvku
        Center=sum(NCoords,2)/4;%Těžiště prvku

        xii=zeros(1,nENodes);
        etai=zeros(1,nENodes);
        dNidxiP=zeros(nENodes,nIntP);
        dNidetaP=zeros(nENodes,nIntP);
        for nN=1:nENodes
            % parametrické funkce
            xii(nN)= 2*(NCoords(1,nN)- Center(1))/(net.hSize);
            etai(nN)=2*(NCoords(2,nN)- Center(2))/(net.vSize);
            for nInt=1:nIntP
                dNidxiP(nN,nInt)=1/4*xii(nN)*(1+etai(nN)*intPoints(nInt,1));
                dNidetaP(nN,nInt)=1/4*etai(nN)*(1+xii(nN)*intPoints(nInt,2));  
            end
        end
        Ke=zeros(length(ENodes)*2);
        for nInt=1:nIntP
            %matrice of shape function derivation
            dNmat{nInt}= [dNidxiP(1,nInt),0,dNidxiP(2,nInt),0,dNidxiP(3,nInt),0,dNidxiP(4,nInt),0;...
                        dNidetaP(1,nInt),0,dNidetaP(2,nInt),0,dNidetaP(3,nInt),0,dNidetaP(4,nInt),0;...
                        0,dNidxiP(1,nInt),0,dNidxiP(2,nInt),0,dNidxiP(3,nInt),0,dNidxiP(4,nInt);...
                        0,dNidetaP(1,nInt),0,dNidetaP(2,nInt),0,dNidetaP(3,nInt),0,dNidetaP(4,nInt)];
            B{nInt}=T*[Jinv,zeros(2);zeros(2),Jinv]*dNmat{nInt};
                    %Gussian integration
            KintP{nInt}=parameters.t*det(J)*B{nInt}'*D*B{nInt};
            Ke=Ke+w(nInt)*KintP{nInt};
        end
        Ke=1/2*(Ke+Ke');
        %lokalizace
        Kd=cell(1,net.nDomain);
        for nD=1:net.nDomain
            DElements=domain(nD).elements;
            Kd{nD}=sparse(zeros(net.nNodes*2)); 
            Kd_preloc{nD}=Kd{nD};
            for nDE=1:domain(nD).nDElements
                Loc=[elements(DElements(nDE)).nodes(1)*2-1,elements(DElements(nDE)).nodes(1)*2,...
                 elements(DElements(nDE)).nodes(2)*2-1,elements(DElements(nDE)).nodes(2)*2,...
                 elements(DElements(nDE)).nodes(3)*2-1,elements(DElements(nDE)).nodes(3)*2,...
                 elements(DElements(nDE)).nodes(4)*2-1,elements(DElements(nDE)).nodes(4)*2];
                 Kd{nD}(Loc,Loc)=Kd{nD}(Loc,Loc)+Ke;
            end
        end
    end
    matrices.Kd_preloc=Kd_preloc;
    matrices.Kd=Kd;
    matrices.Ke=Ke;
end