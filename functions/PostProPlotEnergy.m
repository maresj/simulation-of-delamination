function PostProPlotEnergy(ploting,UseBacktracking,u_d,Energy,material,excercisse,SolutionPath)
    if ploting==1
       fig=figure ;
       hold on
       steps=length(Energy.Sum)-1;
       x=u_d/max(u_d);
       fig=plot(x,Energy.Sum,'color','red','LineWidth',1,'Marker','.','MarkerSize',5);
       fig=plot(x,Energy.E_Om,'color','black','LineStyle','-.');
       fig=plot(x,Energy.E_Gam,'color',[0.3010 0.7450 0.9330],'LineStyle','-.');
       fig=plot(x,Energy.E,'color','blue','LineStyle','-','LineWidth',0.5,'Marker','.','MarkerSize',2);
       fig=plot(x,Energy.D,'color','green','LineStyle','--');
       fig=plot(x,Energy.VarD,'color',[0.9290 0.6940 0.1250],'LineStyle','-','LineWidth',0.5,'Marker','.','MarkerSize',2);
       
     
       t=title('$\textbf{Energy of solutions}$');
       set(t,'Interpreter','latex');
       leg=legend({'$\mathcal{E}_{k}+\sum_{k=1}^{i}\mathcal{D}_{k}$','$\mathcal{E}_{\Omega,k}$','$\mathcal{E}_{\Gamma_{I},k}$','$\mathcal{E}_{k}$','$\mathcal{D}_{k}$','$\sum_{k=1}^{i}\mathcal{D}_{k}$'});
       set(leg,'Interpreter','latex');
       set(leg,'Location','northwest');
       x=xlabel('$\tilde{C}as$');
       y=ylabel('Energie (Nmm)');
       set(x,'Interpreter','latex');
       set(y,'Interpreter','latex');
       
       if UseBacktracking==1
            txt2=' ,Use Backtracking';
       else
            txt2=', No Bactracking';
       end
        
       txt=convertCharsToStrings(SolutionPath)+'Energy material '+material+', excercisse '+excercisse+txt2;
       saveas(fig,txt,'svg');
    end   
end