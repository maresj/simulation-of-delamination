function PostProSolutionVizualization(setings,nNodes,nodes,solution)
if setings.ploting.solution.plot=="yes"
       PlotedSteps=setings.ploting.solution.PlotedSteps;
       DeformScal=setings.ploting.DeformScal;
       PlotedSteps_Num=setings.ploting.solution.PlotedSteps_Num;
       u_max=max(solution.u_d);

       if PlotedSteps=="manual"
          used=PlotedSteps_Num;
       else
          used=[1,5:5:setings.discretization.StepNum];
       end
       nPlot= length(used);
       n=0;
%%     
       for Sn=used
            n=n+1;
            if any(n==1:4:nPlot)
               figure
               t=tiledlayout(4,2);
               title(t,'Deformovaný tvar konstrukce a poškození')
            end
            u_sn=solution.U_t{Sn}; 
            u_dn=-solution.u_d(Sn);
            m=1;
            o=1;

            X1(m,o)=nodes(1).coords(1)+DeformScal*u_sn(1);
            Y1(m,o)=nodes(1).coords(2)+DeformScal*u_sn(2);
            Z1(m,o)=sqrt(u_sn(1)^2+u_sn(2)^2);
            for nN= 2:nNodes
                if nodes(nN-1).coords(2)~=nodes(nN).coords(2)
                   m=m+1;
                   o=0;
                end
                o=o+1;
                if m<=4
                    X1(m,o)=nodes(nN).coords(1)+DeformScal*u_sn(nN*2-1);
                    Y1(m,o)=nodes(nN).coords(2)+DeformScal*u_sn(nN*2);
                    Z1(m,o)=sqrt(u_sn(nN*2)^2+u_sn(nN*2-1)^2);
                end
                if m>4
                    X2(m-4,o)=nodes(nN).coords(1)+DeformScal*u_sn(nN*2-1);
                    Y2(m-4,o)=nodes(nN).coords(2)+DeformScal*u_sn(nN*2);
                    Z2(m-4,o)=sqrt(u_sn(nN*2)^2+u_sn(nN*2-1)^2);
                end
            end
            nexttile
            hold on
            mesh(X1,Y1,Z1)
            mesh(X2,Y2,Z2)
            colormap winter

            txt=['u_{f}=',num2str(u_dn),'m, t=',num2str(-u_dn/u_max)];
            title(txt)
            axis equal
            axis off
            xlim([-0.001, 0.121])
            ylim([-0.025, 0.010])
            xlabel('x [m]')
            ylabel('y [m]')
            set(gca, 'YTickMode', 'manual');
            set(gca, 'YTickLabel', get(gca,'YTick'));

            % Demage on interface
            nexttile
            Om=solution.Om_t{Sn};
            bar(Om);
            xlim([-1, 121])
            xlabel('x [mm]')
            ylabel('\omega(%)')
            xlim([-1, 121])
            ylim([-0.10, 1.10])
            

       end
end