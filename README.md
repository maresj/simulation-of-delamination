# CZ-Simulace delaminace kompozitů založená na variačním modelu poškození
Zde jsou uvedený Matlab script vznikl jako součást mé diplomové práce na téma „Simulace delaminace kompozitů založená na variačním modelu poškození". Řešení problému je založeno na užití algoritmu střídavé minimalizace energie a úpravě řešení s pomocí zpětného trasování energie uvedeného v „Mielke, A., Roubíček, T. a Zeman, J. Complete damage in elastic and viscoelastic media and its energetics".  

V práci je užita diskretizace pomocí metody konečných prků s užitím čtvercových bilineárních konečných prvků a metoda aktivní množiny.

Autor: Bc. Jakub Mareš\
Supervisor: Ing. Martin Doškář, PhD.\
Pracoviště: ČVUT v Praze, Fakulta stavební\
datum: 3. 1. 2024

# Seznam funkcí
 1.  A_MAIN_ACTIVESET_METHOD_FOR_DELAMINATION
 2.  B0_NotEvenSteps
 3.  B1_ExcerciseParameters
 4.  B2_TwoDimensionalExcerciseSquareBeam
 5.  B2_TwoDimensionalExcerciseSquareConsole
 6.  C1_LocalizationNumbers
 7.  C2_OneFiniteElement
 8.  C3_InterfaceStiffnesNumericalIntegration
 9.  C4_InterfaceBooleanMatrix
 10. C5_NeighbourBooleanMatrix
 11. D1_InitialDemageVector
 12. D2_TimeStepAlgorythm 
 13. D2_1_InterfaceStiffnesOpt
 14. D2_2_GlobalMatrix
 15. D2_3_ActiveSetMethod
 16. D2_3_1_ActualizeWorking
 17. D2_3_2_SolveQuadraticSubProblem
 18. D2_4_DisipativeEnergyMinimalizationKonstant
 19. D2_5_ControlDemageChangeKonstant
 20. D2_6_SolveEnergy
 21. D2_7_PlotEnergy
 22. D2_8_backtracking
 23. PostProControlCalculationViz
 24. PostProPlotAnimation
 25. PostProPlotAnimationDemage
 26. PostProPlotBacktracking
 27. PostProPlotEnergy
 28. PostProReactionForcePloting 
 29. PostProSolutionVizualization     

## Návod k obsluze programu
 Pro správnou aktivaci pragramu je doporučeno mít ve vašem zařízení neinstalovaný Matlab R2022, nebo novější.
 Na starší verzích programu Matlab nebyli uvedené funkce testovány. 

 Uložte prosim  všechny soubory ze složky functions do jedné složky ve vašem zařízení včetně prázné složky „solution" do jedné složky ve vašem zařízení tak, aby žádný ze souborů nebyl komprimován.
 Následně program spustíte dvojklikem na soubor A_MAIN_ACTIVESET_METHOD_FOR_DELAMINATION. 

 Před zahájením výpočtu kliknutím na tlačítko „run" prosím zkontrolujte uživatelská nastavení v sekcích „user settings" a „Which plots should be print".

### Uživatelská nastavení 
Nastevení výpočtu jsou shrnuty do struktury „setting", jsou zadávány explicitně v sekcích „usser settins" a „Which plots should be print" v souboru „A_MAIN_ACTIVESET_METHOD_FOR_DELAMINATION“.
Proto zde budeme pro uvádět pouze jednotlivá pole struktury settings, bez části „setting.“.

**material**="ductile", nebo "brittle" \
Nastavení materiálových parametrů. Nastavuje příslušnou sadu materiálových parametrů, které jsou uloženy v materiálové knihovně ve funkci **B1**. 
Do této knihovny je možné přidat další materiál s vlastním volacím jménem.

**excercisse**=1 , nebo 2\
Nastavení geometrie úlohy. Vybírá jendu z geometrií úlohy, které jsou vypočteny funkcemi **B2**. 
Tyto funkce sestavují geometrické informace vždy při spuštění úlohy, lze je nahradit nahráním dat ve vhodném formátu.

**UseBacktracking**=1, nebo 0\
Stanovuje zda má být užito zpětného trasování úlohy (1= ano, 0= ne).

**UpgrageBacktracking**=1, nebo 0\
Určuje zda má být upraveného zpětného trasování energie. Pokud je nastavení 1 musí být nastaveno  **UseBacktracking**=1.

#### Nastavení parametrů časové diskretizace

**discretization.stepsEven**=1, nebo 0\
Nastavení „1“ znamená že bude užita rovnoměrná délka kroku. 
Nastavení „0“ znamená že bude užita nerovnoměrná délka kroku. 

**discretization.TimeStep**= libovolné kladné číslo; [m]\
Udává změnu předpsaných posunů mezi dvěma časovýmí kroky

**discretization.Umin**=libovolné kladné číslo; [m]\
Počáteční hodnota předepsaných posunů v čase t=0. Doporučuji jako počáteční hodnotu volit 0. 

**settings.discretization.StepNum**=libovolné přirozené číslo\
Počet časových kroků, které má algoritmus střídavé minimalizace vypočíst.

**discretization.NotEven.nIntervals**=libovolné přirozené číslo>1\
Udávo počet intervalů na kterých se změní délka časového kroku. Jde tedy o počet různých délek časových kroků. 
Pokud je toto číslo větší než počet zadaných iterací a délek kroku program je předčasně ukončen a vypíše chybové hlášení :„There is more interval iterations settings, than how much intervals we have". 
Pokud je toto číslo naopak menší než počet zadaných délek kroků jsou přebytečné hodnoty ignorovány a uživatel na tento fakt bude upozorněn hlášením. 

**discretization.NotEven.InteravalIterations(i)**=libovolné přirozené číslo>1\
Udává počet iterací provedených na intervalu *i* nerovnoměrného kroku. K tomuto počtu musí být přiřazena hodnota délky kroku. 

**discretization.NotEven.IntervalStepLengthMultiplier(i)**=libovolné nenulové číslo;
Udává délku kroku v intervalu *i* jako násobek hodnoty **discretization.TimeStep**.\
např.: Pokud bude hodnota  **discretization.NotEven.IntervalStepLengthMultiplier(i)=1/2** bude délka kroku v intervalu *i* rovna 1/2***discretization.TimeStep**.

#### Nastavení numerických tolerancí
**numerical.DemageVecTol**= libovolné kladné číslo blížící se nule\
Nastavuje numerickou toleranci pro změnu poškození ve dvou po sobě jdoucích krocích algoritmu střídavé minimalizace. Pokud je změna poškození menší algoritmus je zastaven a přejde do dalšího časového kroku.

**numerical.ActivesetTol**=libovolné kladné číslo blížící se nule\
Natavuje numerickou toleranci pro délku kroku v metodě aktivní množiny. Pokud je délka kroku menší algoritmus přistoupí k ověřování toho zda jsou všechny síly příslušící aktivním podmínkám na rozhraní tlakové.

**numerical.nTol**=libovolné přirozené číslo\
Nastavuje maxiaální množství iterací v metodě střídavé minimalizace pro jeden časový krok. Při překročení této hodnoty bude vypsáno chybové hlášení a algoritmus střídavé minimalizace přejde do dalšího časového kroku bez získání příslušného řešení. 

**numerical.nActive**=libovolné přirozené číslo\
Nastavuje maximální počet kroků v metodě aktivní množiny v rámci jednoho kroku algoritmu střídavé minimalizace.
Při překročení této hodnoty bude vypsáno chybové hlášení a algoritmus střídavé minimalizace přejde do dalšího kroku bez získání příslušného řešení.

**numerical.NumberDeactivated=**'All'(doporučené), nebo libovolný znak ve formě: 'x'
U metody aktivní množiny je možné nastavit různé způsoby odstaňování aktivních podmínek z pracovní množiny. 
Pokud je nastaveno 'All' jsou z pracovní množiny odstraněny všechny podmínky jejichž \alpha_i je menší než \alpha použitá pro délku kroku. Tato možnost však dává smysl pouze pokud některá \alpha_i jsou záporná, což by nemělo nastat.
Pokud je nastaveno cokoli jiného je z pracovní množiny vyřazene vždy pouze nejnižší podmínka.

#### Nastavení geometrické diskretizace úlohy
Není umožněno nerovnoměrné krokování sítě.\
**net.hSize**=vhodné kladné reálné číslo\
nastavuje horizontální velikost konečných prvků užitých pro diskretizaci úlohy
**net.vSize**=vhodné kladné reálné číslo\
nastavuje vertikálníální velikost konečných prvků užitých pro diskretizaci úlohy

### Uživatelská nastavení vykreslování
V této části je umožněno zvolit které grafy a animace mají být na konci výpočtu vykresleny.

**ploting.PlotExcercisse**=1, nebo 0 
Má být vykresleno zadání úlohy? 

#### Vykreslování energie
**ploting.Energy.plot**=1, nebo 0 
Má být na konci výpočtu vykreslen graf energie? Toto nastavení nevypíná výpočet jednotlivých složek energie úlohy.

**ploting.Energy.CalculatioPloting**=0, nebo 1 
Nastavuje zda má být vykreslena energie v každém kroku výpočtu už v průběhu výpočtu. Tato funkce není dobře optimalizována a proto není její užití doporučeno. Zvláště při větším množství časových kroků může dojít k velkému zpomalení výpočtu (Je vykreslováním grafu zaplněna paměť). Slouží pouze pro kontrolu výpočtu. 

#### Vykreslování řešení(deformací a poškození)
 **ploting.solution.plot**="yes", nebo "nop"
 Má být vykresleno deformované řešení? Vykreslí řešení v zadaných krocíkteré jsou určeny v **ploting.solution.PlotedSteps_Num** ( deformace a poškození) v jednotlivých oknech. 

 **ploting.solution.animation**="yes", nebo "nop"
 Nastavuje vykreslení animace deformací v zadaných,  které jsou určeny v **ploting.solution.PlotedSteps_Num** a výslednou animaci uloží do složky definované v **ploting.SolutionPath**.

 **ploting.solution.AnimationFrameRate**= libovolné kladné číslo\
 Určuje počet snímků v uložené animaci za 1 vteřinu. 

 **ploting.AnimationDemage**= 1, nebo 0 
 Nastavuje vykreslení animace výpočtu poškození. Vykreslí hodnoty počkození ve všech iteracích střídavé minimalizace úlohy.

#### kontrola zpětného trasování
**ploting.backtracking**= 1, nebo 0 
Nastavuje vykreslení zpětného trasování. Pokud je rovno 1 je vykreslen oboustraný energetický odhad pro každý časový. 
  
#### Nastavení vykreslování
**ploting.solution.PlotedSteps**='automatic', nebo manual'\ 
Pokud je 'manual' jsou vykreslovány kroky zadané v **ploting.solution.PlotedSteps_Num**, pokud je nastaveno 'automatic' je vykreslován každý pátý krok. 

**ploting.solution.PlotedSteps_Num**=[vektor přirozených čísel]\
Určuje vektor časových kroků které mají být vykresleny při manuálním nastavení. 

**settings.ploting.DeformScal**=přirozené číslo\
Určuje škálování vykreslovaných deformací. Deformace jsou touto hodnotou přenásobeny. 

**settings.ploting.SolutionPath**='adressa určité složky ve vašem zařízení'\
Určuje složku, do které budou ukládány výsledné obrázky a animace. V základu je zde určena složka 'solution\', což je prázdná složka která je součástí souborů programu.

#### Nastavení vykreslování kalkulace
Toto nastevení se týká kontrolního vykreslování konkrétních iterací, které je umožněno ve funkci *PostProControlCalculationViz*

**ploting.calc.plotedSteps.c**= vektor přirozených čísel\
**ploting.calc.plotedSteps.j**= vektor přirozených čísel\
**ploting.calc.plotedSteps.i**= vektor přirozených čísel\
Určují které iterace mají být vykresleny z následujících možností.  **.c** určuje, které časové kroky. **.j** určuje, které iterace ve střídavé minimalizaci budou vykresleny v každém zadaném časovém kroku 
a **.i** určuje které iterace metody aktivní množiny budou vykresleny v každé zvoleném kroku střídavé minimalizace. 

**settings.ploting.calc.workingSet**=0, nebo 1\
Umožňuje vykreslení pracovní množiny ve zvolených iteracích

**ploting.calc.SumWorking**=0, nebo 1 \
umožňuje vykreslení počtu aktivních podmínek v zadaných iteracích.

**ploting.calc.StepLength**=0, nebo 1 \
umožňuje vykreslení vývoje délky kroku aktivní množiny v ve zvolených iteracích metody střídavé minimalizace.

**ploting.calc.Lam**=0, nebo 1\
Umožňuje vykreslení sil na rozhraní ve zvolených iteracích.

**ploting.calc.iterations**=1, nebo 0\
vykreslí trojrozměrný graf zachycující počty provedených iterací v každém časovém kroku. 


## Krátký popis jednotlivých funkcí
### 1.  A_MAIN_ACTIVESET_METHOD_FOR_DELAMINATION 
 Hlavní script, který slouží pro uživatelské nastavení a spoušťení funkcí

### 2.  B0_NotEvenSteps
 Tvoří vektor všech časových kroků zatěžovacích posunů při nerovnoměrné délce kroku 

### 3.  B1_ExcerciseParameters
 Slouží jako materiálová knihovna. Slouží zápis informací o vlastnostech rozhraní materiálu.

### 4.  B2_TwoDimensionalExcerciseSquareBeam a   B2_TwoDimensionalExcerciseSquareConsole
Vytváří geometrii úlohy. Bodovou síť. Informace o konečných prvcích. Místa předepsaných posunů. Rozdělení na oblasti

### 6.  C1_LocalizationNumbers
Vytváří vektory čísel pro lokalizaci vektorů posunů a pro vyřazení předepsaných nulových posunů z matic. 

### 7.  C2_OneFiniteElement
Tvoří matici tuhosti materiálu úlohy jako lokalizaci jednotlivých prvkových matic. V tomto scriptu jsou považovány všechny matice prvků totožné a matice tuhosti je tudíž tvořena lokalizací jediného prvku.

### 8.  C3_InterfaceStiffnesNumericalIntegration
Tvoří prvkové matice integrálů z bázových funkcí, z níchž je následně tvořena matice tuhosti na rozhraní. 
V této matici není zavedena tuhost, která závisí na poškození, která je vypočtena později.

### 9.  C4_InterfaceBooleanMatrix
Definuje Booleovskou matici podmínek na rozhraní.

### 10. C5_NeighbourBooleanMatrix
Připraveno pro výpočty s doménovou dekompozicí. Slouží pro tvorbu Booleovské matice podmínek na rozhraní. Tyto matice jsou v tomto skriptu prázdné a slouží pouze pro správnou funkci celého skriptu. 

### 11. D1_InitialDemageVector
Definuje počáteční vektor poškození v čase t=0. 

### 12. D2_TimeStepAlgorythm 
Algoritmus střídavé minimalizace energie. 

### 13. D2_1_InterfaceStiffnesOpt
Tvoří matici na rozhraní z matic integrací bázových funkcí dřívwe získaných.

### 14. D2_2_GlobalMatrix
Globalizuje jednotlivé matice tuhosti do globální matice tuhosti.

### 15. D2_3_ActiveSetMethod
Algoritmus metody aktivní množiny

### 16. D2_3_1_ActualizeWorking
Funkce která aktualizuje Booleovské matice jen na podmínky, které jsou aktivní.

### 17. D2_3_2_SolveQuadraticSubProblem
Nachází vektor kroku metody aktivní množiny, jako řešení kvadratického podproblému.

### 18. D2_4_DisipativeEnergyMinimalizationKonstant
Nachází vektor poškození jako bod pro který je nulová derivace celkové energie podle poškození. 

### 19. D2_5_ControlDemageChangeKonstant
Provádí kontrolu zda změna poškození je větší, než povolená tolerance.

### 20. D2_6_SolveEnerg
Provádí výpočet jednotlivých složek energie.

### 21. D2_7_PlotEnergy
Vykresluje energii v a ktuálním časovém kroku. Slouží pouze pro kontrolu a při větším množství iterací značné zahlcuje paměť. Je doporučeno tuto funkci neužívat (nastavit **ploting.Energy.CalculatioPloting**=0)

### 22. D2_8_backtracking
Provádí kontrolu oboustraného energetického odhadu. Oboustraný energetický odhad je zde možné převést na skutečný odhad energie v každém časovém kroku. Získané hodnoty následně mohou být vykresleny.

### 23. PostProControlCalculationViz
Slouží pro vykreslování různých hodnot pro kontrolu správnosti výpočtu (např.: délka kroku aktivní množiny ve zvolených iteracích, počet aktivních podmínek atd.)

### 24. PostProPlotAnimation
Vykreslí animaci deformací a poškození v předepsaných časových krocích a uloží ji do určené složky

### 25. PostProPlotAnimationDemage
Vykreslí animaci vývoje poškození ve všech iteracích metody střídavé minimalizace

### 26. PostProPlotBacktracking
Vykreslí oboustraný energetický odhad ve všech časových krocích.

### 27. PostProPlotEnergy
Výkreslí vývoj jednotlivých složek energie

### 28. PostProReactionForcePloting
Vykreslí reakce v místě předepsaného posunu v závislosti na tomto posunu.

### 29. PostProSolutionVizualization     
Vykreslí deformace a poškození v určitých časových krocích.


# EN-Simulation of delamination in composites based on a variational damage model
These are all Matlab scripts needed for delamination simulation implemented for my thesis "Simulace delaminace kompozitů založená na variačním modelu poškození".  
The problem solution is based on the alternating minimalization algorythm and correction of the solution using the heuristic energy backtracking presented by A. Mielke, T. Roubiček and J. Zeman  

Within work is used Active set algorithm and finite element method with square finite elements.

Author: Bc. Jakub Mareš\
Supervisor: Ing. Martin Doškář, PhD.\
Institution: CTU in Prag, Fakulty of civil engeniering\
date: 3. 1. 2024


## list of scripts

 1.  A_MAIN_ACTIVESET_METHOD_FOR_DELAMINATION
 2.  B0_NotEvenSteps
 3.  B1_ExcerciseParameters
 4.  B2_TwoDimensionalExcerciseSquareBeam
 5.  B2_TwoDimensionalExcerciseSquareConsole
 6.  C1_LocalizationNumbers
 7.  C2_OneFiniteElement
 8.  C3_InterfaceStiffnesNumericalIntegration
 9.  C4_InterfaceBooleanMatrix
 10. C5_NeighbourBooleanMatrix
 11. D1_InitialDemageVector
 12. D2_TimeStepAlgorythm 
 13. D2_1_InterfaceStiffnesOpt
 14. D2_2_GlobalMatrix
 15. D2_3_ActiveSetMethod
 16. D2_3_1_ActualizeWorking
 17. D2_3_2_SolveQuadraticSubProblem
 18. D2_4_DisipativeEnergyMinimalizationKonstant
 19. D2_5_ControlDemageChangeKonstant
 20. D2_6_SolveEnergy
 21. D2_7_PlotEnergy
 22. D2_8_backtracking
 23. PostProControlCalculationViz
 24. PostProPlotAnimation
 25. PostProPlotAnimationDemage
 26. PostProPlotBacktracking
 27. PostProPlotEnergy
 28. PostProReactionForcePloting
 29. PostProSolutionVizualization     

 <!--## Scripts description and user manual
 ### 1. A_MAIN_ACTIVESET_METHOD_FOR_DELAMINATION.m
 Main script with several sections

**User settings** - there are calculation setings\
**Which plots should be print**- setting for ploting, useful mainly for algorythm testing.\
**Preprocessing**- In this section are defined all material and geometry informations and from these informations are made all basic stiffnes matrices and boundary conditions boolean matrices.\
**Initialization**- Theres initialized alternating minimalization algorithm.\
**Calculation**- In this section are calculated all displacement, demages and energies.\
**Postprocessing**- There are all ploting functions\

#### **User settings** 

### 2. B0_NotEvenSteps
### 3. B1_ExcerciseParameters
### 4. B2_TwoDimensionalExcerciseSquareBeam 
### B0_NotEvenSteps
### B0_NotEvenSteps
### B0_NotEvenSteps
### B0_NotEvenSteps
 -->
